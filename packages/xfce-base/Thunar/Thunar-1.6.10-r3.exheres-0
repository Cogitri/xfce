# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2009 Elias Pipping <elias@pipping.org>
# Copyright 2011 Mickaël Guérin <kael@crocobox.org>
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'thunar-0.9.0-r2.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

OPTION_RENAMES=(
    'apr-plugin plugins:apr'
    'trash-plugin plugins:trash'
    'uca-plugin plugins:uca'
    'wallpaper-plugin plugins:wallpaper'
)

require option-renames
require xfce [ intltoolize=false ]

SUMMARY="Modern, fast and easy-to-use file manager for Xfce"
HOMEPAGE="http://docs.xfce.org/xfce/${PN,,}/start"

SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    dbus
    debug
    exif [[ description = [ Allow reading EXIF metadata ] ]]
    libnotify [[ description = [ Enable mount notifications ] ]]
    pcre [[ description = [ Allow usage of regular expressions for bulk renaming ] ]]
    startup-notification
    plugins:
        apr         [[ description = [ Advanced file properties plugin ] ]]
        trash       [[ requires = dbus description = [ Trash panel applet ] ]]
        uca         [[ description = [ Custom actions plugin ] ]]
        wallpaper   [[ description = [ Wallpaper setting plugin ] ]]

    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"
# gtk-doc building is currently broken; http://git.xfce.org/xfce/thunar/commit/configure.ac.in?id=d1110b89b3b9553e05329f7293d1b8998894e5ba

DEPENDENCIES="
    build+run:
        dev-libs/atk
        dev-libs/glib:2[>=2.30]
        dev-util/desktop-file-utils
        media-libs/freetype:2
        media-libs/libpng[>=1.2]
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0[>=2.14]
        x11-libs/gtk+:2[>=2.24]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/pango
        x11-misc/shared-mime-info
        xfce-base/exo[>=0.10.0]
        xfce-base/libxfce4ui[>=4.10.0]
        xfce-base/libxfce4util[>=4.10.0]
        xfce-base/xfconf[>=4.10.0]
        dbus? ( dev-libs/dbus-glib[>=0.34] )
        exif? ( media-libs/libexif[>=0.6.0] )
        libnotify? ( x11-libs/libnotify[>=0.4.0] )
        pcre? ( dev-libs/pcre[>=6.0] )
        startup-notification? ( x11-libs/startup-notification[>=0.4] )
        plugins:trash? ( xfce-base/xfce4-panel[>=4.7.0] )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        providers:ijg-jpeg? ( media-libs/jpeg )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    suggestion:
        xfce-extra/thunar-archive-plugin [[
            description = [ Thunar can handle archives in the context menus with thunar-archive-plugin ]
        ]]
        xfce-extra/thunar-media-tags-plugin [[
            description = [ Thunar can view and edit music metadata with thunar-media-tags-plugin ]
        ]]
        xfce-extra/thunar-volman [[
            description = [ Automatic management of removable media ]
        ]]
"
#build:
#    gtk-doc? ( dev-doc/gtk-doc )

RESTRICT="test" # tests need X access

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-gudev
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    dbus
    debug
    exif
    'libnotify notifications'
    pcre
    startup-notification
    'plugins:apr apr-plugin'
    'plugins:trash tpa-plugin'
    'plugins:uca uca-plugin'
    'plugins:wallpaper wallpaper-plugin'
)

